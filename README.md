# storeden-laravel-api-connettor

# Warning: this packaging is in development mode!

An API Storeden SDK for Laravel.

This package satisfy **Authorization Endpoint** required in online Storeden setting customized app profile

See: [Storeden doc](https://developers.storeden.com/docs#box1)

## Usage

Add Storeden Laravel API connector to your project:

```
composer require velvetmedia/storeden-laravel-api-connettor
```

See into routes.php for getting right routes to insert in the Storeden setting form

## Iframe and X-Frame-Options 

Storeden use iframe tag for injected your app in his backoffice.

So, if you use Forge, edit Nginx file like this:

```
add_header X-Frame-Options "allow-from https://[UUID]-backoffice.storeden.com/";
```

where [UUID] is you Merchant id, otherwise if your app is developed for any stores:

```
add_header X-Frame-Options "ALLOWALL";
```

## Middleware

Add a new middleware to kernel.php in your app, like:

```
protected $routeMiddleware = [
    'auth.sdk.storeden' => \Velvetmedia\StoredenLaravelAPIConnector\Middleware\AuthSdkStoredenMiddleware::class,
    ...
];
```

Use like this:

```
Route::get('products', 'ProductsController@index')->middleware('auth.sdk.storeden');
```

### Note

A middleware, need var **instance** into query string. Provide your route like this:

```
<a href="{{ route('storeden.endpoint.embed.test', ['instance' => request()->get('instance')]) }}">EMBED TEST</a>
```

## Custom embed welcome views

For customizing this package views launch command:

```
php artisan vendor:publish
```

And select 

```
Provider: Velvetmedia\StoredenLaravelAPIConnector\StoredenApiConnectorServiceProvider
```

Overwrite route for "embed" endpoint, like:

```
// Overwrite original route
Route::get('endpoint/embed', function () {
    return view('storeden.welcome');
})->middleware('auth.sdk.storeden');
```

Or with another custom controller and another custom view.

## Access to API

Once activated, the middleware can be accessed by request, like:

```
public function embed(Request $request)
{
    // SDK connection by middleware
    return $request->sdk->api('/store/info.json', 'GET');
}
```