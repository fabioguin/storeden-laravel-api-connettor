<?php

namespace Velvetmedia\StoredenLaravelAPIConnector;

use Illuminate\Support\ServiceProvider;

class StoredenApiConnectorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/views', 'storeden-laravel-api-connettor');
        $this->publishes([
            __DIR__ . '/views' => base_path('resources/views/storeden'),
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // 
        $this->app->make('Velvetmedia\StoredenLaravelAPIConnector\Controllers\EndpointsController');
    }
}
