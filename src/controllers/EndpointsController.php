<?php
/**
 * This package satisfy Authorization Endpoint required in
 * online Storeden setting customized app profile
 * See: https://developers.storeden.com/docs#box1
 *
 * Storeden SDK is taken from
 * https://github.com/storeden/connect-api-sdk
 *
 * @author Fabio Guin
 *
 */

namespace Velvetmedia\StoredenLaravelAPIConnector\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Velvetmedia\StoredenLaravelAPIConnector\Models\StoredenApp;
use Velvetmedia\StoredenLaravelAPIConnector\SDK\Storeden;

class EndpointsController extends Controller
{
    /**
     * Authorization Endpoint
     * Satisfy "Authorization URL"
     *
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function auth(Request $request)
    {
        // Retrieve post request by Storeden
        $data = [
            'instance' => $request->instance,
            'app_key' => $request->key,
            'app_secret' => $request->exchange,
        ];
        // Simply save or can i have a response? I don't know
        $storeden_app = StoredenApp::create($data);
    }

    /**
     * Authorization Endpoint
     * Satisfy "De-authorization URL"
     *
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function deauth(Request $request)
    {
        // Delete store
        $storeden_app = StoredenApp::where('instance', $request->instance)->delete();
    }

    /**
     * Authorization Endpoint
     * Satisfy "Embed URL"
     *
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function embed(Request $request)
    {
        // SDK connection by middleware
        $group = $request->sdk->api('/store/info.json', 'GET');
        // Set layout
        return $this->layout($request, $group);
    }

    /**
     * Authorization Endpoint
     * Satisfy "Storefront URL"
     *
     * @param  Illuminate\Http\Request  $request
     * @param  Velvetmedia\StoredenLaravelAPIConnector\SDK\Storeden  $group
     * @return  void
     */
    public function layout(Request $request, $group)
    {
        // Test response
        $text = 'UUID: ' . $group->response->uid . '<br /> Store name: ' . $group->response->store_name;

        return view('storeden-laravel-api-connettor::welcome', compact('text'));
    }

    /**
     * Authorization Endpoint
     * Satisfy "Webhook URL"
     *
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function webhook(Request $request)
    {
        //
        return 'i\'am webhook';
    }

}
