<?php
/**
 * Utilities by Storeden
 *
 * @author Fabio Guin
 *
 */

namespace Velvetmedia\StoredenLaravelAPIConnector\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Velvetmedia\StoredenLaravelAPIConnector\Models\StoredenApp;
use Velvetmedia\StoredenLaravelAPIConnector\SDK\Storeden;

class UtilitiesController extends Controller
{
    /**
     * https://developers.storeden.com/docs/platform
     * Get platform status
     * Get information about platform status and api available versions
     * 
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function platform(Request $request)
    {
        // SDK connection by middleware
        $res = $request->sdk->api('/platform', 'GET');
        //$res = json_decode($res, true);

        dd($res);
    }

    /**
     * https://developers.storeden.com/docs/platform
     * Get platform status
     * Get information about platform status and api available versions
     * 
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function connection(Request $request)
    {
        $store = $request->sdk->api('/store/info.json', 'GET');
        // SDK connection by middleware
        $res = $request->sdk->api('/connection', 'GET', $store->response->uid);
        //$res = json_decode($res, true);

        dd($res);
    }

    /**
     * Connect API Rate Limiting
     *
     * All Storeden Connect API has a rate limit of 100.000 single requests every 24 hours.
     * When you reach the Connect API limit, any api calls will receive an error code 509.
     * You can check the number of calls remaining by fetch
     * https://connect.storeden.com/unversioned/connection ( authentication required ).
     * This API call is not subject to throttling.
     *
     * We can't using AuthSdkStoredenMiddleware so, we using CURL
     *
     * @param  Illuminate\Http\Request  $request
     * @return  void
     */
    public function monitor(Request $request)
    {
        $storeden_app = StoredenApp::where('instance', $request->instance)->first();

        // create a new cURL resource
        $ch = curl_init();

        // Auth
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'key: ' . $storeden_app->app_key,
            'exchange: ' . $storeden_app->app_secret,
        ));

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, "https://connect.storeden.com/unversioned/connection");

        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // grab URL and pass it to the browser
        $results = curl_exec($ch);
        $results = json_decode($results, true);

        // close cURL resource, and free up system resources
        curl_close($ch);

        /**
         * Output example
         *
         *  array:4 [▼
         *       "throttle_limit" => 1000
         *       "current_usage" => array:3 [▼
         *           "value" => 0
         *           "errors" => 0
         *           "percentage" => "0%"
         *       ]
         *       "ratios" => array:2 [▼
         *           "success" => "0%"
         *           "error" => "0%"
         *       ]
         *      "client" => array:2 [▼
         *           "sdk" => "browser"
         *           "ip" => "138.68.86.131"
         *       ]
         *  ]
         */

        // Set layout
        return view('storeden-laravel-api-connettor::monitor', compact('results'));
    }

    /**
     * ONLY FOR TEST
     * https://developers.storeden.com/docs/products
     */
    public function testProducts(Request $request)
    {
        $products = $request->sdk->api('/products/list.json', 'GET');
        //dd($products);
        return view('storeden-laravel-api-connettor::products', compact('products'));
    }

}
