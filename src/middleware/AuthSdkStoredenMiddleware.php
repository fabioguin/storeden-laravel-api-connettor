<?php

namespace Velvetmedia\StoredenLaravelAPIConnector\Middleware;

use Velvetmedia\StoredenLaravelAPIConnector\Models\StoredenApp;
use Velvetmedia\StoredenLaravelAPIConnector\SDK\Storeden;
use Closure;

class AuthSdkStoredenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $storeden_app = StoredenApp::where('instance', $request->instance)->first();

        $request->sdk = new Storeden([
            'api_key' => $storeden_app->app_key,
            'api_exchange' => $storeden_app->app_secret, 
            'api_version' => 'v1.1',
        ]);

        return $next($request);
    }
}
