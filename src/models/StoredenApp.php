<?php

namespace Velvetmedia\StoredenLaravelAPIConnector\Models;

use Illuminate\Database\Eloquent\Model;

class StoredenApp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'storeden_app';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'instance',
        'app_key',
        'app_secret',
    ];
}
