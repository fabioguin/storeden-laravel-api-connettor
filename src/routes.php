<?php
use Velvetmedia\StoredenLaravelAPIConnector\Middleware\AuthSdkStoredenMiddleware;
// Enpoints needs HTTPS

Route::group([
    'namespace' => 'Velvetmedia\StoredenLaravelAPIConnector\Controllers',
    'prefix' => 'endpoint',
], function () {

    // "Authorization URL" endpoint
    Route::post('auth', 'EndpointsController@auth');
    // "De-authorization URL" endpoint
    Route::post('deauth', 'EndpointsController@deauth');
    // "Embed URL" endpoint
    Route::get('embed', 'EndpointsController@embed')
        ->name('storeden.endpoint.embed')
        ->middleware(AuthSdkStoredenMiddleware::class);
    // "Storefront URL" endpoint
    Route::get('layout', 'EndpointsController@layout');
    // "Webhook URL" endpoint
    Route::get('webhook', 'EndpointsController@webhook')
        ->middleware(AuthSdkStoredenMiddleware::class);

}); 

// Utilities

Route::group([
    'namespace' => 'Velvetmedia\StoredenLaravelAPIConnector\Controllers',
    'prefix' => 'utilities',
], function () {

    // Check the number of calls remaining by fetch
    Route::get('statistic', 'UtilitiesController@monitor')
        ->name('storeden.utilities.monitor');
    Route::get('platform', 'UtilitiesController@platform')
        ->name('storeden.utilities.platform')
        ->middleware(AuthSdkStoredenMiddleware::class);
    Route::get('connection', 'UtilitiesController@connection')
        ->name('storeden.utilities.connection')
        ->middleware(AuthSdkStoredenMiddleware::class);
    // Products list, ONLY FOR TESTING
    Route::get('test-products', 'UtilitiesController@testProducts')
        ->name('storeden.utilities.testproducts')
        ->middleware(AuthSdkStoredenMiddleware::class);

});
