<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .t-green {
                color: green;
            }

            .t-red {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">

                <div class="links m-b-md">
                    <a href="{{ route('storeden.endpoint.embed', ['instance' => request()->get('instance')]) }}">Home</a>
                    <a href="{{ route('storeden.utilities.testproducts', ['instance' => request()->get('instance')]) }}">Test Prodotti</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>

                <div class="title m-b-md">
                    Monitor connessioni
                </div>

                <div class="m-b-md">
                    
                    <progress value="{{ $results['current_usage']['value'] }}" max="{{ $results['throttle_limit'] }}"></progress> {{ $results['current_usage']['percentage'] }}

                    <p>
                        Limite richieste 24/h: <big>{{ $results['throttle_limit'] }}</big><br />
                        <span class="t-green">Utilizzate: <big>{{ $results['current_usage']['value'] }}</big> ({{ $results['ratios']['success'] }})</span><br />
                        <span class="t-red">errori: <big>{{ $results['current_usage']['errors'] }}</big>  ({{ $results['ratios']['error'] }})</span>
                    </p>
                </div>

            </div>
        </div>
    </body>
</html>
